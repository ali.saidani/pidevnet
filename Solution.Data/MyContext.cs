﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using Solution.Domain.Entities;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solution.Data.Configurations;

namespace Solution.Data
{
    public class MyContext:DbContext
    {
        public MyContext() : base("Name=DefaultConnection")
        {
            Database.SetInitializer<MyContext> (new DropCreateDatabaseIfModelChanges<MyContext>());

        }
        
        public DbSet<employe> employes { get; set; }
        public DbSet<Entreprise> Entreprises { get; set; }
      
       
        //DbSet<> ...
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            // config + customConventions
            /*modelBuilder.Configurations.Add(.....);
            modelBuilder.Conventions.Add(....);*/
            
           //modelBuilder.Configurations.Add(new ChefprojetConfiguration());
           modelBuilder.Entity<Abonnement>().ToTable("Abonnement");
           modelBuilder.Entity<Activite>().ToTable("Activite");
           modelBuilder.Entity<ChefEquipe>().ToTable("ChefEquipe");
          
           modelBuilder.Entity<employe>().ToTable("employe");
           modelBuilder.Entity<OffreEmploi>().ToTable("OffreEmploi");
           modelBuilder.Entity<RH>().ToTable("RH");
           modelBuilder.Entity<Reclamation>().ToTable("Reclamation");
           modelBuilder.Entity<Like>().ToTable("Like");
           modelBuilder.Entity<Post>().ToTable("Post");
           modelBuilder.Entity<Messagerie>().ToTable("Messagerie");
           modelBuilder.Entity<Commentaire>().ToTable("Commentaire");
           modelBuilder.Entity<Notification>().ToTable("Notification");
           modelBuilder.Entity<User>().ToTable("User");
           modelBuilder.Entity<Education>().ToTable("Education");
           modelBuilder.Entity<Experience>().ToTable("Experience");
           modelBuilder.Entity<Candidate>().ToTable("Candidate");






       }
   }
}
 //sakrou aslan projet w aawed helou trah