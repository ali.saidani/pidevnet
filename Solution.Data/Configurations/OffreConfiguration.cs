﻿using Solution.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Data.Configurations
{
 public class OffreConfiguration : EntityTypeConfiguration<OffreEmploi>
    {
        public OffreConfiguration()
        {
            HasOptional(c => c.Entreprise).
                    WithMany(e => e.offers).
                    HasForeignKey(f => f.OffreId).
                    WillCascadeOnDelete(false);
        }
    }
}
