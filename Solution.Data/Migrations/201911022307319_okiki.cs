namespace Solution.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class okiki : DbMigration
    {
        public override void Up() 
        {
            CreateTable(
                "dbo.User",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        username = c.String(),
                        email = c.String(),
                        password = c.String(),
                        phone = c.String(),
                        address = c.String(),
                        picture = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Entreprises",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                        Address = c.String(),
                        UrlImage = c.String(),
                        dateCreation = c.DateTime(nullable: false),
                        fondateur = c.String(),
                        categorie = c.String(),
                        Email = c.String(),
                        RHId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.RH", t => t.RHId)
                .Index(t => t.RHId);
            
            CreateTable(
                "dbo.OffreEmploi",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        OffreId = c.Int(),
                        description = c.String(),
                        datePub = c.DateTime(nullable: false),
                        niveauExp = c.String(),
                        logo = c.String(),
                        categorie = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Entreprises", t => t.OffreId)
                .Index(t => t.OffreId);
            
            CreateTable(
                "dbo.Abonnement",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        idUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Activite",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        description = c.String(),
                        dateEvent = c.DateTime(nullable: false),
                        type = c.String(),
                        picture = c.String(),
                        place = c.String(),
                        enterprise = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Reclamation",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        idUser = c.Int(nullable: false),
                        date = c.Int(nullable: false),
                        message = c.String(),
                        raison = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Like",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_post = c.Int(nullable: false),
                        id_user = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Post",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nom_post = c.String(),
                        description = c.String(),
                        date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Messagerie",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        idUser = c.Int(nullable: false),
                        date = c.Int(nullable: false),
                        message = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Commentaire",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        idUser = c.Int(nullable: false),
                        idPost = c.Int(nullable: false),
                        description = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Notification",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        idUser = c.Int(nullable: false),
                        date = c.Int(nullable: false),
                        contenu = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Education",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        diploma = c.String(),
                        institution = c.String(nullable: false),
                        description = c.String(),
                        startDate = c.DateTime(nullable: false),
                        endDate = c.DateTime(nullable: false),
                        CandidatId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Candidate", t => t.CandidatId)
                .Index(t => t.CandidatId);
            
            CreateTable(
                "dbo.Candidate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        firstName = c.String(),
                        lastName = c.String(),
                        userName = c.String(),
                        password = c.String(nullable: false),
                        email = c.String(nullable: false),
                        phoneNumber = c.String(nullable: false),
                        introduction = c.String(),
                        certification = c.String(),
                        skills = c.String(),
                        address = c.String(nullable: false),
                        creationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Experience",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        workPlace = c.String(nullable: false),
                        campany = c.String(nullable: false),
                        description = c.String(),
                        startDate = c.DateTime(nullable: false),
                        endDate = c.DateTime(nullable: false),
                        CandidatId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Candidate", t => t.CandidatId)
                .Index(t => t.CandidatId);
            
            CreateTable(
                "dbo.employe",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        post = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.ChefEquipe",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        enterprise = c.String(),
                        equipe = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.RH",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        enterprise = c.String(),
                        grade = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.ID)
                .Index(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RH", "ID", "dbo.User");
            DropForeignKey("dbo.ChefEquipe", "ID", "dbo.User");
            DropForeignKey("dbo.employe", "ID", "dbo.User");
            DropForeignKey("dbo.Experience", "CandidatId", "dbo.Candidate");
            DropForeignKey("dbo.Education", "CandidatId", "dbo.Candidate");
            DropForeignKey("dbo.Entreprises", "RHId", "dbo.RH");
            DropForeignKey("dbo.OffreEmploi", "OffreId", "dbo.Entreprises");
            DropIndex("dbo.RH", new[] { "ID" });
            DropIndex("dbo.ChefEquipe", new[] { "ID" });
            DropIndex("dbo.employe", new[] { "ID" });
            DropIndex("dbo.Experience", new[] { "CandidatId" });
            DropIndex("dbo.Education", new[] { "CandidatId" });
            DropIndex("dbo.OffreEmploi", new[] { "OffreId" });
            DropIndex("dbo.Entreprises", new[] { "RHId" });
            DropTable("dbo.RH");
            DropTable("dbo.ChefEquipe");
            DropTable("dbo.employe");
            DropTable("dbo.Experience");
            DropTable("dbo.Candidate");
            DropTable("dbo.Education");
            DropTable("dbo.Notification");
            DropTable("dbo.Commentaire");
            DropTable("dbo.Messagerie");
            DropTable("dbo.Post");
            DropTable("dbo.Like");
            DropTable("dbo.Reclamation");
            DropTable("dbo.Activite");
            DropTable("dbo.Abonnement");
            DropTable("dbo.OffreEmploi");
            DropTable("dbo.Entreprises");
            DropTable("dbo.User");
        }
    }
}
