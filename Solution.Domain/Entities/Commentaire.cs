﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Commentaire
    {
        public int id { get; set; }
        public int idUser { get; set; }
        public int idPost { get; set; }
        public int description { get; set; }
    }
}
