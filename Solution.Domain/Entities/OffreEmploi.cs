﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
   public class OffreEmploi
    {
        public int id { get; set; }
        public int? OffreId { get; set; }
        [ForeignKey("OffreId")]
        public virtual Entreprise Entreprise { get; set; }
        public string description { get; set; }
        public DateTime datePub { get; set; }
        public String  niveauExp{ get; set; }
        public string logo{ get; set; }
        public string categorie { get; set; }
    }
}
