﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class ChefEquipe:User
    {
        public Entreprise enterprise { get; set; }
        public string equipe { get; set; }
    }
}
