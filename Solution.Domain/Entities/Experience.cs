﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Experience
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string workPlace { get; set; }
        [Required]
        public string campany { get; set; }
        public string description { get; set; }
        [Required]
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public int? CandidatId { get; set; }
        [ForeignKey("CandidatId")]
        public virtual Candidate candidat { get; set; }
    }
}
