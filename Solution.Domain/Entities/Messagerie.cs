﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Messagerie
    {
        public int id{ get; set; }
        public int idUser { get; set; }
        public int date { get; set; }
        public string message { get; set; }
    }
}
