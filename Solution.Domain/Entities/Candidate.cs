﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Candidate
    {
        [Key]
        public int Id { get; set; }

        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }


        [Required]
        [DataType(DataType.Password)]
        [MinLength(8)]
        public string password { get; set; }

        [Required]
        [NotMapped]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string confirmPassword { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [Required]
        public string phoneNumber { get; set; }
        public string introduction { get; set; }
        public string certification { get; set; }
        public string skills { get; set; }

        [Required]
        public string address { get; set; }
        public DateTime creationDate { get; set; }

        public virtual ICollection<Education> Educations { get; set; }
        public virtual ICollection<Experience> Experiences { get; set; }
    }
}
