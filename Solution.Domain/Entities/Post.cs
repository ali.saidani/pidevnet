﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Domain.Entities
{
    public class Post
    {
        public int id { get; set; }
        public string nom_post { get; set; }
        public string description { get; set; }
        public DateTime date{ get; set; }
        
    }
}
