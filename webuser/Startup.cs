﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webuser.Startup))]
namespace webuser
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
