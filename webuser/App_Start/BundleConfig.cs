﻿using System.Web;
using System.Web.Optimization;

namespace webuser
{
    public class BundleConfig
    {
        // Pour plus d'informations sur le regroupement, visitez https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilisez la version de développement de Modernizr pour le développement et l'apprentissage. Puis, une fois
            // prêt pour la production, utilisez l'outil de génération à l'adresse https://modernizr.com pour sélectionner uniquement les tests dont vous avez besoin.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css"));

            bundles.Add(new ScriptBundle("~/bundles/frontJs").Include(
                      "~/Scripts/jquery-3.4.1.min.js",
                      "~/Scripts/jquery-migrate-3.1.0.min.js",
                      "~/Scripts/custom.js",
                      "~/Scripts/jquery.superfish.js",
                      "~/Scripts/jquery.themepunch.tools.min.js",
                      "~/Scripts/jquery.themepunch.revolution.min.js",
                      "~/Scripts/jquery.themepunch.showbizpro.min.js",
                      "~/Scripts/jquery.flexslider-min.js",
                      "~/Scripts/chosen.jquery.min.js",
                      "~/Scripts/jquery.magnific-popup.min.js",
                      "~/Scripts/jquery.counterup.min.js",
                      "~/Scripts/jquery.jpanelmenu.js",
                      "~/Scripts/stacktable.js",
                      "~/Scripts/slick.min.js",
                      "~/Scripts/headroom.min.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/frontCss").Include(
                      "~/Content/style.css",
                      "~/Content/colors.css"));
        }
    }
}
