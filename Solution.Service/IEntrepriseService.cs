﻿using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public interface IEntrepriseService : IService<Entreprise>
    {
        IEnumerable<Entreprise> GetEntrepriseById(int id);
     
    }
}
