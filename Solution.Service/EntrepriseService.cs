﻿using Service.Pattern;
using Solution.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Service
{
    public class EntrepriseService:Service<Entreprise>,IEntrepriseService
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork UTK = new UnitOfWork(factory);
        IDataBaseFactory factor = null;
        private int id;

        public EntrepriseService():base(UTK)
        {

        }



     
        IEnumerable<Entreprise> IEntrepriseService.GetEntrepriseById(int id)
        {
            return GetMany(e => e.id == id);
        }
    }
}
